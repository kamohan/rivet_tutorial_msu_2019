#!/bin/bash
docker run -it --rm hepstore/rivet:2.7.2 bash
ctrl-p+q
export RIVET_ANALYSIS_PATH=$PWD
rivet-buildplugin Rivet_my_analysis.so my_analysis.cc
rivet --list-analyses --analysis-path=$PWD
rivet tag_1_pythia8_events.hepmc.gz --pwd -a my_analysis
rivet-mkhtml --mc-errs Rivet.yoda:"MC Simulation"
docker run -it --rm hepstore/rivet:2.7.2 bash
